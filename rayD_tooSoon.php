<?
	ob_start();
	session_start();
	$rnd=1;
	include('includes/connection.php');
	$curdate=date('y/m/d h:i:s');
	$ip=$_SERVER['REMOTE_ADDR'];
	$sess_id=session_id();
	$insess=mysql_query("insert into sessions (user_id, ip,started_timestamp) values('$sess_id', '$ip', '$curdate')");
	//require_once("facebook_bootstrap.php");
?>

<!DOCTYPE html>
<html>
	<head>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script type="text/javascript" src="//use.typekit.net/ysj4ovc.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		<script type="text/javascript" src="http://fast.fonts.net/jsapi/df5fbc32-ec3c-4146-8bef-4e529f9c0412.js"></script>
		<script src="js/input.labels.js"></script>
		<script src="js/jquery.cycle.lite.js"></script>

		<link rel="stylesheet" href="css/rayDonovan.css" />
		<script src="js/rayDonovan.js"></script>

		<title>Showtime's Ray Donovan - LA Life Sweepstakes</title>
	</head>
	<body>
		<div id="modal" style="display:block; opacity:0.85"></div>

		<div id="bodyContent">
			<div class="header">
				<div class="socialIcons">
					<a href="#" id="twitIcon"><img src="images/twitterIcon.png" /></a>
					<a href="#" id="fbIcon"><img src="images/facebookIcon.png" /></a>
					<a href="#" id="pinterestIcon"><img src="images/pinterestIcon.png" /></a>
					<a href="#" id="gplusIcon"><img src="images/gPlusIcon.png" /></a>
				</div>
			</div>

			<div id="infoSection">
			
				<div class="teaserBar">
					<h2 class="headline redShadow">Live Like Ray Donovan - Minus the drama.</h2>
					<h5 class="basicText">Enter today for the chance to win an exclusive trip to Los Angeles.</h5>
					<div class="rdButton" id="enterNow"><div class="btnText">Enter Now</div></div>
				</div>
				
				<div class="prize">
					<div class="col1 prizeDesc">
						<div class="headline">Grand-Prize Trip for two includes:</div>
						<ul>
							<li class="basicText">Round-trip airfare to Los Angeles, California</li>
							<li class="basicText">Luxury accommodations at Hotel Mondrian for 3 nights</li>
							<li class="basicText">Dinner at Chateau Marmont</li>
							<li class="basicText">$3,000 Rodeo Drive shopping spree</li>
							<li class="basicText">Helicopter tour</li>
							<li class="basicText">$1,000 spa credit</li>
						</ul>
						<div class="headline subHead">Weekly Winners Will receive a ray donovan prize bundle.</div>
					</div>
					<div class="col2" id="picRotate">
						<img src="images/airTravel.jpg" height=343 width=364 />
						<img src="images/helicopter.jpg" height=343 width=364 />
						<img src="images/spa.jpg" height=343 width=364 />
						<img src="images/dining.jpg" height=343 width=364 />
						<img src="images/rodeo.jpg" height=343 width=364 />
						<img src="images/pool.jpg" height=343 width=364 />
					</div>
				</div>

				<div class="attractBar">
					<img src="images/attractBanner_beforePremiere.png" />
				</div>

				<div class="upgradeOrWatch">
					<div class="upgrade">
						<div class="headlineLight red">Order Showtime now and get <span class="headline">50% off for 12 months, </span></div>
						<div class="basicText">then catch up on Ray Donovan on <br />SHOWTIME ON DEMAND&reg; and SHOWTIME ANYTIME&reg; <br /><b>FREE</b> with your subscription.</div>
						<div class="rdButton" id="upgradeNow"><div class="btnText"><a href="http://www.verizon.com/home/fiostv/web/showtime">Upgrade Now</a></div></div>
					</div>
					<div class="watch">
						<div class="headlineLight red">Watch Free episodes of Ray Donovan</div>
						<div class="basicText">Watch episodes from the first season of Ray Donovan <b>FREE</b> and then order in time for Season 2 premiere on July 13th, 9pm ET/PT.</div>
						<a href="https://apps.facebook.com/nowonfios/television/showtime" target="_blank">
							<div class="rdButton" id="watchNow">
								<img src="images/watchNow.png" />
								<div class="btnText">Watch Now</div>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="footer basicText">
				<div class="links">
					<a href="includes/rules.php" target="_blank" id="officialRules">Official Rules</a>
					<a href="includes/privacyPolicy.html" id="privacyPolicy">Privacy Policy</a>
				</div>
				<div class="rules">
					NO PURCHASE/SUBSCRIPTION NECESSARY TO ENTER OR WIN. A PURCHASE/SUBSCRIPTION WILL NOT INCREASE YOUR CHANCES OF WINNING. Begins at 12:00:00am ET on 6/30/14 and ends at 11:59:59pm ET on 7/27/14. Consists of 4 weekly periods beginning at 12:00:00am ET each Monday during the sweepstakes period and ending the following Sunday at 11:59:59pm ET, each with a weekly prize. Entries roll over from week to week and will be included in the grand prize trip drawing. Open to legal residents of any 1 of the 48 contiguous US or DC who are at least 21 years old and who are Verizon residential customers and the named account holders in good standing as of June 30, 2014. Void in Alaska, Hawaii, and where prohibited by law. Subject to full Official Rules available at www.raydonovanlalife.com/rules. ARV of each weekly prize pack: $210. ARV of the grand prize trip: $10,000. Total ARV of all prizes offered: $10,840. GRAND PRIZE WINNER AND GUEST MUST TRAVEL ON SPONSOR-SELECTED DATES BEFORE DECEMBER 31,                  2014 TO AND FROM SPONSOR-SELECTED AIRPORT, OR PRIZE WILL BE FORFEITED. Blackout dates apply. Prize details may vary. Odds of winning depend on the # of eligible entries received for that drawing.                       Sponsor: Showtime Networks, Inc., 1633 Broadway, NY, NY 10019. SHOWTIME&reg; and related marks are trademarks of Showtime Networks Inc., a CBS company. Ray Donovan&copy; Showtime Networks Inc., All rights reserved. Limited-time offer for new SHOWTIME customers. Customers with existing premium channel package promotions will only receive discount for the remainder of the original term of the existing promotion which may be less than 12 months.  Discount applied via bill credits for promotional period (up to 12 months); beginning the first month after the promotional period ends, standard programming rates apply. Additional promotion eligibility rules may apply. Equipment & other charges, taxes & terms apply. &copy; 2014 Verizon.
				</div>
			</div>
		</div>

		<div id="tooSoon" class="overlay" style="display:block;">
			<img src="images/entryForm_header.jpg" />

			<div class="middleThird">
				<div class="tooSoonAlert basicText">
					Thank you for your interest in the Ray Donovan L.A. Life Sweepstakes. Please come back starting June 30th for your daily chances to win!
				</div>
			</div>

			<div class="attractBar">
				<img src="images/popup_attractBar.jpg" />
			</div>
		</div>

	</body>
</html>