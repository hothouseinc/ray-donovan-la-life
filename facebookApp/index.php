<?
	ob_start();
	session_start();
	$rnd=1;
	include('includes/connection.php');
	$curdate=date('y/m/d h:i:s');
	$ip=$_SERVER['REMOTE_ADDR'];
	$sess_id=session_id();
	$insess=mysql_query("insert into sessions (user_id, ip,started_timestamp) values('$sess_id', '$ip', '$curdate')");
	//require_once("facebook_bootstrap.php");
?>

<!DOCTYPE html>
 <html itemscope itemtype="http://schema.org/Article">
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		
		<!-- FONTS -->
		<script type="text/javascript" src="https://use.typekit.net/ysj4ovc.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		<script type="text/javascript" src="https://fast.fonts.net/jsapi/df5fbc32-ec3c-4146-8bef-4e529f9c0412.js"></script>
		
		<script src="js/input.labels.js"></script>
		<script src="js/jquery.cycle.lite.js"></script>
		<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>

		<link rel="stylesheet" href="css/rayDonovan.css" />
		<title>Showtime's Ray Donovan - LA Life Sweepstakes</title>

		<meta itemprop="name" content="Win a luxury trip for two to L.A.">
		<meta itemprop="description" content="Enter at www.RayDonovanLALife.com for a chance to experience the perks of being a Hollywood fixer. You could win a VIP trip for two to L.A. from from Showtime® &amp; Verizon FiOS.">
		<meta itemprop="image" content="http://raydonovanlalife.com/images/gplus_a.jpg">
	</head>
	<body>
		<div id="modal"></div>

		<div id="bodyContent">
			<div class="header">
				<div class="socialIcons">
					<a href="#" id="twitIcon"><img src="images/twitterIcon.png" /></a>
					<a href="#" id="fbIcon"><img src="images/facebookIcon.png" /></a>
					<a href="#" id="pinterestIcon"><img src="images/pinterestIcon.png" /></a>
					
					<a href="https://plus.google.com/share?url=http://raydonovanlalife.com/rayD.php" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
						<img src="images/gPlusIcon.png" />
					</a>
					<!--
					<script src="https://apis.google.com/js/plusone.js"></script>
					<g:plus action="share"></g:plus>
					-->
				</div>
			</div>

			<div id="infoSection">
			
				<div class="teaserBar">
					<h2 class="headline" id="liveLikeRay"></h2>
					<h5 class="basicText">Enter today for the chance to win an exclusive trip to Los Angeles.</h5>
					<div class="rdButton" id="enterNow"><div class="btnText">Enter Now</div></div>
				</div>
				
				<div class="prize">
					<div class="col1 prizeDesc">
						<div id="grandPrize"></div>
						<ul>
							<li class="basicText">Round-trip airfare to Los Angeles, California</li>
							<li class="basicText">Luxury accommodations at Hotel Mondrian for 3 nights</li>
							<li class="basicText">Dinner at Chateau Marmont</li>
							<li class="basicText">$3,000 Rodeo Drive shopping spree</li>
							<li class="basicText">Helicopter tour</li>
							<li class="basicText">$1,000 spa credit</li>
						</ul>
						<div id="weeklyWinners"></div>
					</div>
					<div class="col2" id="picRotate">
						<img src="images/airTravel.jpg" height=320 width=337 />
						<img src="images/pool.jpg" height=320 width=337 />
						<img src="images/dining.jpg" height=320 width=337 />
						<img src="images/rodeo.jpg" height=320 width=337 />
						<img src="images/helicopter.jpg" height=320 width=337 />
						<img src="images/spa.jpg" height=320 width=337 />
					</div>
				</div>

				<div class="attractBar">
					<img src="images/attractBanner_beforePremiere.png" />
				</div>

				<div class="upgradeOrWatch">
					<div class="upgrade">
						<div id="orderShowtime"></div>
						<div class="basicText">then catch up on Ray Donovan on <br />SHOWTIME ON DEMAND&reg; and SHOWTIME ANYTIME&reg; <br /><b>FREE</b> with your subscription.</div>
						<div class="rdButton" id="upgradeNow"><div class="btnText"><a id="upgradeNow" href="http://www.verizon.com/home/fiostv/web/showtime">Upgrade Now</a></div></div>
					</div>
					<div class="watch">
						<div id="watchFree"></div>
						<div class="basicText">Watch episodes from the first season of Ray Donovan <b>FREE</b> and then order in time for Season 2 premiere on July 13th, 9pm ET/PT.</div>
						<a id="watchNow" href="https://apps.facebook.com/nowonfios/television/showtime" target="_blank">
							<div class="rdButton" id="watchNow">
								<img src="images/watchNow.png" />
								<div class="btnText">Watch Now</div>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="footer basicText">
				<div class="links">
					<a href="includes/rules.php" target="_blank" id="officialRules">Official Rules</a>
					<a href="http://www.sho.com/sho/about/privacy" id="privacyPolicy">Privacy Policy</a>
				</div>
				<div class="rules">
					NO PURCHASE/SUBSCRIPTION NECESSARY TO ENTER OR WIN. A PURCHASE/SUBSCRIPTION WILL NOT INCREASE YOUR CHANCES OF WINNING. Begins at 12:00:00am ET on 6/30/14 and ends at 11:59:59pm ET on 7/27/14. Consists of 4 weekly periods beginning at 12:00:00am ET each Monday during the sweepstakes period and ending the following Sunday at 11:59:59pm ET, each with a weekly prize. Entries roll over from week to week and will be included in the grand prize trip drawing. Open to legal residents of any 1 of the 48 contiguous US or DC who are at least 21 years old and who are Verizon residential customers and the named account holders in good standing as of June 30, 2014. Void in Alaska, Hawaii, and where prohibited by law. Subject to full Official Rules available at www.raydonovanlalife.com/rules. ARV of each weekly prize pack: $210. ARV of the grand prize trip: $10,000. Total ARV of all prizes offered: $10,840. GRAND PRIZE WINNER AND GUEST MUST TRAVEL ON SPONSOR-SELECTED DATES BEFORE DECEMBER 31,                  2014 TO AND FROM SPONSOR-SELECTED AIRPORT, OR PRIZE WILL BE FORFEITED. Blackout dates apply. Prize details may vary. Odds of winning depend on the # of eligible entries received for that drawing.                       Sponsor: Showtime Networks, Inc., 1633 Broadway, NY, NY 10019. SHOWTIME&reg; and related marks are trademarks of Showtime Networks Inc., a CBS company. Ray Donovan&copy; Showtime Networks Inc., All rights reserved. Limited-time offer for new SHOWTIME customers. Customers with existing premium channel package promotions will only receive discount for the remainder of the original term of the existing promotion which may be less than 12 months.  Discount applied via bill credits for promotional period (up to 12 months); beginning the first month after the promotional period ends, standard programming rates apply. Additional promotion eligibility rules may apply. Equipment & other charges, taxes & terms apply. &copy; 2014 Verizon.
				</div>
			</div>
		</div>

		<div id="entryForm" class="overlay">
			<div class="closeBtn"></div>
			<img src="images/entryForm_header.jpg" />

			<div class="middleThird">
				<form id="entry">
					<input type="hidden" id="sessid" name="sessid"  value="<?=$sess_id;?>" />
		            <input type="hidden" id="sessid_r" name="sessid_r"  value="<?=$sess_id;?>" />
		            <input type="hidden" id="ip" name="ip"  value="<?=$ip;?>" />

					<div class="row1 row">
						<label class="input">
							<span>First Name / Last Name</span>
							<input type="text" id="name" />
						</label>

						<label class="input">
							<span>E-Mail</span>
							<input type="text" id="email" />
						</label>
					</div>
					<div class="row2 row">						
						<label class="input">
							<span>Phone Number</span>
							<input type="text" id="phone" />
						</label>

						<input type="radio" id="agree" />
						<label for="agree">I agree to the <a href="includes/rules.php" target="_blank" id="officialRules">Official Rules</a></label>
					</div>
					<div class="row3 row">
						<div id="errorMessage"></div>
					</div>
					<div class="rdButton" id="entryForm_enterNow">Enter Now</div>
				</form>
			</div>

			<div class="attractBar">
				<img src="images/popup_attractBar.jpg" />
			</div>
		</div>

		<div id="thankYou" class="overlay">
			<div class="closeBtn"></div>
			<img src="images/thankYou_header.jpg" />

			<div class="middleThird">
				<div class="basicText flavor" id="preClick">
					<h2 class="basicText">Want more chances to win?</h2>
					Come back daily for your chance to win the grand prize. Plus get an automatic bonus entry today when you click below to learn more about SHOWTIME ANYTIME&reg;.
				</div>

				<div class="basicText flavor" id="postClick">
					<h2 class="basicText">You've claimed a bonus entry!</h2>
					Be sure to come back tomorrow to claim another entry.
				</div>

				<div class="rdButton" id="getMyBonusEntry">Get My Bonus Entry</div>

				<div class="ty_socialIcons">
					<a href="#" id="ty_Twitter"><img border=0 src="images/thankYou_twitterIcon.png" /></a>
					<a href="#" id="ty_Facebook"><img border=0 src="images/thankYou_facebookIcon.png" /></a>
				</div>
			</div>

			<div class="attractBar">
				<img src="images/popup_attractBar.jpg" />
			</div>
		</div>

		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId      : '1440585286200758', // App ID
					channel    : 'https://hothousesecure.com/raylalife/channel.php',	
					status     : true, // check login status
					cookie     : true, // enable cookies to allow the server to access the session
					xfbml      : true,  // parse XFBML
					oauth : true
				});
				FB.Canvas.setSize();
				FB.Canvas.setAutoGrow();	
			};

			(function(d){
				var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement('script'); js.id = id; js.async = true;
				js.src = "//connect.facebook.net/en_US/all.js";
				ref.parentNode.insertBefore(js, ref);
			}(document));
		</script> 

		<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49932329-4', 'hothousesecure.com');
		  ga('send', 'pageview');

		</script>
		<script src="js/rayDonovan.js"></script>
</body>
</html>