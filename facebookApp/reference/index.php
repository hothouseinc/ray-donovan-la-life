<?
ob_start();
session_start();
$rnd=1;
include('includes/connection.php');
$curdate=date('y/m/d h:i:s');
$ip=$_SERVER['REMOTE_ADDR'];
$sess_id=session_id();
$insess=mysql_query("insert into sessions (user_id, ip,started_timestamp) values('$sess_id', '$ip', '$curdate')");
require_once("facebook_bootstrap.php");
$sumsql=mysql_query("SELECT sum(votes) as VOTES FROM `teams` where round='$rnd'");
$sqle=mysql_query("select * from teams where conf='east' and round='$rnd' order by auto_id");
$sqlw=mysql_query("select * from teams where conf='west' and round='$rnd' order by auto_id");
$sqler=mysql_query("select * from teams where conf='east' and round='$rnd' order by auto_id");
$sqlwr=mysql_query("select * from teams where conf='west' and round='$rnd' order by auto_id");
$sumrow=mysql_fetch_array($sumsql);
$votes=($sumrow['VOTES']);
//echo "votes is ".$votes;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Quest for the Cup Sweepstakes</title>

<link rel="stylesheet" type="text/css" href="css/bright_nhlstyle.css">
<script type="text/javascript" src="//use.typekit.net/stk7hus.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript" src="js/vendor/modernizr-latest.js"></script>
<script> if ( ! window.console ) console = { log: function(){} };</script>
</head>
<body>
<div id="container">
<?php
if(!$page_liked ){
	echo "<script>console.log('not liked');</script>";

	echo '<div id="like-gate"> <img src="images/bright_likegate.jpg" alt="" /></div>';
}
?>
<div id="rules-modal"><div class="close-pop butn"></div>
<br><div id="rztxt">
<?php
	include 'includes/rules.php';
?></div>
</div>
  <header id="page-hdr">
  <article id="logo">

      <img src="images/player_1.png" alt="pl1" class="rotating-item" width="190" height="200" />
      <img src="images/player_2.png" alt="pl2" class="rotating-item" width="190" height="200" />
      <img src="images/player_3.png" alt="pl3" class="rotating-item" width="190" height="200" />
      <img src="images/player_4.png" alt="pl4" class="rotating-item" width="190" height="200" />
</article>
   <article id="logo2"><img src="images/bright_hdr_logos.png" alt="" /></article>
  </header>
  <div id="content">
    <section id="sweeps">
    	<section id="landing"  >
      <div id="info" class="mod active">
      <h1>CALLING ALL<br>HOCKEY FANS</h1>
      <p>The road to the Stanley Cup is long and hard &#8212; <br>and victory is sweet. NBCSN is celebrating the <br>fans that root for their team all season long.</p> 
<p><b>Enter for the chance to win autographed<br>hockey memorabilia or our grand-prize trip<br>to the hockey-loving town of Toronto.</b></p>
      <a href="#" class="btn" id="begin-btn">Enter Now</a> 
      
      </div>
      <div id="prz"><img src="images/toronto.jpg"><h2>Grand-Prize Trip Includes:</h2>
      <ul> <li><span>Round-trip airfaire for two to Toronto, Ontario</span></li>
       <li><span>3-night/4-day stay in a downtown Toronto hotel</span></li>
       <li><span>Tickets to hockey-themed attractions</span></li>
       </ul>
       </div>
       <div style="clear:both"></div>
       </section>
       <div id="entry" class="mod">
  
        <h1>Enter Now</h1>
         
        <form id="entry-form" name="entry-form">
          <fieldset>
            <input type="hidden" id="sessid" name="sessid"  value="<?=$sess_id;?>" >
            <input type="hidden" id="sessid_r" name="sessid_r"  value="<?=$sess_id;?>" >
            <input type="hidden" id="ip" name="ip"  value="<?=$ip;?>" >
            <label>First Name</label><label>Last Name</label>
            <input type="text" name="fname" id="fname" title="First name is required" required />
            <input type="text" name="lname" id="lname" title="Last name is required"  required />
            <label>Email</label>
            <input type="email" name="email" id="email"  title="Email is required" required />
            <label>Date of Birth (MM-DD-YYYY)</label><label>Phone No (###-###-####)</label>
            <input type="text" name="dob" id="dob"  title="Date of Birth is required" required />
            <input type="text" name="phone" id="phone"  title="10-Digit Phone Number is required" required />
          </fieldset>
          <fieldset>
          <p>
              <input type="checkbox" id="rules" name="rules" title="You must accept the official rules" required />
              I agree to the <a href="#rules" class="rules-trigger">Official Rules</a> and am a Bright House Networks customer.</p>
              </fieldset>
            <a href="#" class="btn" id="submit-btn">SUBMIT</a>
        </form>
        <div id="error-container">
          <h4>Please correct the following errors:</h4>
        </div>
      </div>
      <div id="thanks">
        <h1>Thank you</h1>
        <h2>For entering the<br>Quest for the cup sweepstakes!</h2>
        <h4>Come back tomorrow for another chance<br> to win<span id='bnscpy'> or score an additional entry now by<br>voting for your Stanley Cup hopeful below.</span>
        <!--<span id='bnscpy'>.  You scored an additional entry today by<br>voting for your Stanley Cup hopeful below.</span>-->
       </h4>
       
        <div id='share'> <div id="sharefbsw" class="butn"></div><div id="sharetwsw" class="butn"></div> </p>
      </div>
    </section>
    <div id="poll">
    <h1 >who will win it all?</h1><p>Tell us who you think will win the Stanley Cup and see how many people are voting for your team<br>with our interactive poll. <b>Select your favorite team below that will make it all the way to the Cup.</b></p>
    <div id='pollstart'>
    <div id="eastconf">
        <? while($sqlrow=mysql_fetch_array($sqle)){ ?>
		<li id="<?=$sqlrow['team'];?>" class="team<?=$sqlrow['status'];?> <?=$sqlrow['conf'];?>" ><span><?=$sqlrow['team'];?></span>
        <div style=" <?=$sqlrow['css']." margin: ".$sqlrow['top']."px 0px 0px ".$sqlrow['lr']."px;";?>position:absolute; "></div>
        </li>
		
		<? }?>
	</div>
     <div id="westconf">
        <? while($sqlrow=mysql_fetch_array($sqlw)){ ?>
		<li id="<?=$sqlrow['team'];?>" class="team<?=$sqlrow['status'];?> <?=$sqlrow['conf'];?>"><span><?=$sqlrow['team'];?></span>
        <div style=" <?=$sqlrow['css']." margin: ".$sqlrow['top']."px ".$sqlrow['lr']."px 0px 0px;";?>float:right; "></div>
        </li>
		
		<? }?>
    </div>
    
	</div>
    <div id="pollafter">
	<div id="eastconf">
        <? while($sqlrow=mysql_fetch_array($sqler)){ 

		?>
		<li id="<?=$sqlrow['auto_id'];?>_result" class="teamresult <?=$sqlrow['conf'];?>">
        <span class="resulteast" >0%</span>
        <div id="<?=$sqlrow['auto_id'];?>_bar" class="pollbareast" ></div>
        <div style=" <?=$sqlrow['css']." margin: ".$sqlrow['top']."px 0px 0px ".$sqlrow['lr']."px;";?>position:absolute; "></div>
        </li>
		
		<? }?>
	</div>
     <div id="westconf">
        <? while($sqlrow=mysql_fetch_array($sqlwr)){
         ?>
		<li id="<?=$sqlrow['auto_id'];?>_result" class="teamresult <?=$sqlrow['conf'];?>">
        <span class="resultwest" >0%</span>
        <div id="<?=$sqlrow['auto_id'];?>_bar" class="pollbarwest" ></div>
        <div class="icon" style=" <?=$sqlrow['css']." margin: ".$sqlrow['top']."px ".$sqlrow['lr']."px 0px 0px;";?> "></div>
        </li>
		
		<? }?>
        </div>
	</div>
     
     
    </div>
  </div>
  <footer id="page-ftr">
  <p><a href="#rules" class="rules-trigger">Official Rules</a> | <a href="http://www.nbcuni.com/privacy/" target="_blank">Privacy Policy</a></p>
<h6>NO PURCHASE NECESSARY TO ENTER OR WIN. Void where prohibited. Legal residents of the 50 US or DC who are 18+ and are existing customers of<br>Bright House Networks. Begins 5/1/14 at 1:00 PM ET and ends 5/31/14 at 11:59 PM ET. For odds and prize descriptions, see Official Rules above.</h6>
  </footer>
</div>
<div id='yourteam'>
<div class="close-pop butn"></div>
<div id="pickleft" ><img src="images/teams/288anaheim_ducks.png" id="yourlogo_lg">
<div id="leftbot">
		<li id="yourteam_poll" class="teamresult east">
        <span id="yourspan" class="resulteast" >0%</span>
        <div id="yourteam_bar" class="pollbareast" ></div>
       <!-- <div id="yourlogo_sm" style=" <?=$sqlrow['css']." margin: ".$sqlrow['top']."px 0px 0px ".$sqlrow['lr']."px;";?>position:absolute; "></div>-->
       <div id="yourlogo_sm" style=" background: url('images/sprite_03.png') no-repeat 0 -546px;width: 45px; height: 32px; margin: -6px 0px 0px -22px;position:absolute; "></div>
       <div style=""></div>
        </li>
</div>
</div>
<div id="pickright" ><div class='prh2'>YOU PICKED THE</div><h1>BOSton<br>Bruins</h1>
<p>See how many people agree with you to the left and share your prediction with friends. Then watch the action unfold on NBCSN's Stanley Cup playoff coverage. </p>
<div id="sharefb" class="butn"></div><div id="sharetw" class="butn"></div>
</div></div>
<div id="fb-root" style="position:relative; display:none;"></div>
<script>
window.fbAsyncInit = function() {
	FB.init({
		appId      : '1440585286200758', // App ID
		channel    : 'https://hothousesecure.com/raylalife/channel.php',	
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true,  // parse XFBML
		oauth : true
	});
	
	FB.Canvas.setSize();
	FB.Canvas.setAutoGrow();
	
	
};
	
(function(d){

	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	
	if (d.getElementById(id)) {return;}
	
	js = d.createElement('script'); js.id = id; js.async = true;
	
	js.src = "//connect.facebook.net/en_US/all.js";
	
	ref.parentNode.insertBefore(js, ref);

}(document));
</script> 

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
<!--
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script src="js/vendor/jquery.placeholder.js"></script> 
--><script src="js/vendor/date.js"></script> 
<script src="js/vendor/jquery.validate.js"></script> 
<script type="text/javascript" src="js/nhl.bright.js"></script> 
<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49932329-4', 'hothousesecure.com');
  ga('send', 'pageview');

</script>
</body>
</html>