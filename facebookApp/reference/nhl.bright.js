jQuery.noConflict();
var nhl = nhl || {};
var mvpd='bright';
var sharepk='';
var sharemasc='';
var shid='';
var bitly = 'http://qftcbrighthouse.com';
var rooturl='https://hothousesecure.com/nhl/'+mvpd;
var entryrow='';
var bns='';



nhl.bright = (function($) {
	function initialize() {
		//$('input, textarea').placeholder();
		//$('.close').on('click', closeModal);
		$('.rules-trigger').on('click', toggleRules);
		
		//$('.close-pop, .close').on('click', toggleContent);
		$('#submit-btn').on('click', submitForm)
		$('#begin-btn').click(function(){
			ga('send', 'event', 'Button', 'EntryStart');
        startSubmit ();
    	});
		$('#sharefbsw').click(postToWall);
		$('#sharetwsw').click(setTweetsw);
		$('#sharefb').click(postToWallpoll);
		$('#sharetw').click(function(){
			setTweet('poll');
		}
		);
		$('.teamin').click(function(){
		 		$(this).addClass('onFocus');
   			 var currentId = $(this).attr('id');
			pollResult(currentId);
		}
		);
		$('.close-pop').click(function(){
			$('#yourteam').fadeOut('fast');
			$('#rules-modal').fadeOut('fast');
		}
		);
		
		validateEntryForm();
		//setTweet();
		initTracking();
		    var InfiniteRotator =
    {
        init: function()
        {
            //initial fade-in time (in milliseconds)
            var initialFadeIn = 1000;
 
            //interval between items (in milliseconds)
            var itemInterval = 5000;
 
            //cross-fade time (in milliseconds)
            var fadeTime = 2500;
 
            //count number of items
            var numberOfItems = $('.rotating-item').length;
 
            //set current item
            var currentItem = 0;
 
            //show first item
            $('.rotating-item').eq(currentItem).fadeIn(initialFadeIn);
 
            //loop through the items
            var infiniteLoop = setInterval(function(){
                $('.rotating-item').eq(currentItem).fadeOut(fadeTime);
 
                if(currentItem == numberOfItems -1){
                    currentItem = 0;
                }else{
                    currentItem++;
                }
                $('.rotating-item').eq(currentItem).fadeIn(fadeTime);
 
            }, itemInterval);
        }
    };
 
    InfiniteRotator.init();
	}
	function startSubmit(){
		$('#landing').fadeOut('fast',function(){$('#entry').fadeIn('fast');});
		//).css({zIndex:99}).fadeOut
	}
	function pollResult(team) {
		console.log('team is '+ team);
		var data='team='+team +'&entryrow='+entryrow;
		
		$.ajax({
			url: "processPoll.php",	
			type: "GET",
			data: data,		
			dataType: "json",			
			cache: false,
			success: function (html) { 
        		bns=1;
				console.log("html is "+html);
				output=html;
				var newcss=output.bgstyle;
				var margin="margin:"+output.topstyle+"px 0px 0px "+output.lrstyle+"px;";
				
				document.getElementById("yourlogo_sm").setAttribute("style",newcss+margin);
				sharepk=output.picked;
				sharemasc=output.masc;
				lgimg="images/teams/team_"+output.tmid+".png";
				shid=output.tmid;
				ga('send', 'event', 'Poll', output.tmname);
				$("#pickright h1").html(output.tmname);
				$("#yourlogo_lg").attr("src",lgimg);
				var tmnum=parseFloat(output.tmperc).toFixed(1);
				$("#yourspan").html(tmnum+"%&nbsp;");
				//add in percentage to popup
				pollinf=output.pollinfo;
				
				$('#pollstart').fadeOut('fast',function(){$('#pollafter').fadeIn('slow',
				function(){
					$.each(pollinf, function() {
 					var tm=this.auto_id;
					var cf=this.conf;
					var pnum=parseFloat(this.perc).toFixed(1);
					var barsz=parseInt(pnum)/100*135;
					$("#"+tm+"_result .result"+cf).html(pnum+"%");
					showBar(tm, barsz);
	       		});
				}
				);});
				
				
				
				$('#yourteam').fadeIn('6000');
				//console.log(html);
    		}			

		});
	}
	function showBar(barid, barsz){
		$('#'+barid+"_bar").animate({width:"+="+barsz+"px"},{duration:600, queue:false});	
	}
function scrollTo(y){
    FB.Canvas.getPageInfo(function(pageInfo){
            $({y: pageInfo.scrollTop}).animate(
                {y: y},
                {duration: 1000, step: function(offset){
                    FB.Canvas.scrollTo(0, offset);
                }
            });
    });
}
	function toggleRules() {
		ga('send', 'event', 'Button', 'Rules');
		$('#rules-modal').toggle();
		scrollTo(200);
		return false;
	}
	function initTracking() {
		trackIt('tw-thx');
		trackIt('fb-thx');
	}
	function trackIt(id) {
		var $this = $('#' + id);
		$this.click( function() { trackDiv(id); } )
	}
	function trackDiv(divtrk){
		console.log(divtrk + ' clicked');
		var zess =document.getElementById("sessid_r").value;
		//alert(zess);
		var data = 'sess=' + zess + '&trk=' + divtrk;
		$.ajax({
			url: "processtrk.php",	
			type: "GET",
			data: data,					
			cache: false,			
			success: function (html) {
				console.log('successfully tracked.');			
			}
		});	
		//alert (data);
		/**/
	}
	
	function setTweet(poll) {
		
		twUrl = 'https://twitter.com/intent/tweet?via=BrightHouseNow&text=';
		//msg = 'at - %40 ampersand %26 hashtag %23';
		if(poll){
			ga('send', 'event', 'Tweet', 'Poll_'+sharemasc);
			msg = 'I picked the '+sharemasc+' to win the %23StanleyCup. Visit @NBCSN Quest for the Cup page to vote. ' + bitly;
			msg = msg.split(' ').join('%20');
			var  newwindow = window.open(twUrl + msg, "_blank");
			//$('#sharetw').attr('href', twUrl + msg);
			
		}
	}
	function setTweetsw() {
		ga('send', 'event', 'Tweet', 'Sweeps');
		twUrl = 'https://twitter.com/intent/tweet?via=BrightHouseNow&text=';
			msg = 'Love %23hockey? Celebrate the game with a trip to Toronto! Enter @NBCSN Quest for the Cup Sweeps: ' + bitly;
			msg = msg.split(' ').join('%20');
			var  newwindow = window.open(twUrl + msg, "_blank");

		//console.log(msg);
		
		
	}

	function postToWallpoll() {
		console.log('bitly is'+bitly);
		ga('send', 'event', 'FBShare', 'Poll_'+sharemasc);
		//var shimg=$("#yourlogo_lg").attr('src');
		FB.ui({
			'method': 'feed',
			'link': bitly,
			'picture': rooturl+'/images/teams/team_'+shid+'.png',
			'name': sharepk+' '+sharemasc+' will win the Stanley Cup!',
			'description': 'I just picked the '+sharemasc+' to win it all. Visit NBCSN&#39;s Quest for the Cup page and vote for the team that you think will win this year&#8212;and see who agree with you. Plus enter for a chance to win a hockey lover&#39;s getaway to Toronto. http://on.fb.me/1o6PTUI'     					
		}),
		function(response) {
			console.log(response);	
		};

		return false;
	}
	function postToWall() {
		ga('send', 'event', 'FBShare', 'Sweeps');
		FB.ui({
			'method': 'feed',
			'link': bitly,
			'picture': rooturl+'/images/'+mvpd+'_fb_icon.jpg',
			'name': 'Calling all Hockey Fans',
			'description': 'NBCSN is celebrating all things hockey&#8212;including fans&#8212;with the Quest for the Cup Sweepstakes. I entered for a chance to win autographed memorabilia or even a hockey lover’s dream trip to Toronto. Enter now and you could win big. http://on.fb.me/1o6PTUI'
		}),
		function(response) {
			console.log(response);	
		};
		
		return false;
	}
	
	
	
	function toggleContent() {
		var content = $(this).attr('href');
		console.log(content);
		$('.active').hide();
		$(content).fadeIn().addClass('active');
		return false;
	}
	
	function showThanks() {
		$('#entry').fadeOut('fast',function(){$('#thanks').fadeIn('fast');});
		/*$('#entry').fadeOut();
		$('#thanks').fadeIn().addClass('active');
		*/return false;
	}
	
	function submitForm() {
		$('#entry-form').submit();	
		return false;
	}
	
	function showError(jqXHR,textStatus,errorThrown){
		alert('Error');
		console.log("\nerror: " + jqXHR +  " " + textStatus + " " + errorThrown);
		console.log(jqXHR.responseText);
	}
	
	function validateEntryForm() {
		jQuery.validator.addMethod("dateCheck", function(value, element) { 
			return Date.parseExact(value, "M-d-yyyy");
		});
		
		$.validator.addMethod("ageCheck", function(value, element) {
			var dob = $('#dob').val();
			dob = Date.parse(dob);
			var yrsAgo =Date.parse('-18years') ;
			
			return (dob <= yrsAgo);
		
		}, "You must be at least 18 years of age.");
		$.validator.addMethod("phoneCheck", function(value, element) {
			var phone = $('#phone').val();
                 return phone.match(/[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})|\d{10}/);
            	//return false;
			}, "Please enter phone number as ###-###-####.");

		$("#entry-form").validate({
			errorLabelContainer: $("#error-container"),
			
			rules: {
				dob: {
					dateCheck: true,
					ageCheck: true,
				},
				phone: {
					phoneCheck: true
				}
			},
			
			messages: {
				email: {
					email: 'Please enter a valid email address.',
				},
				dob: {
					dateCheck: 'Please enter date in the format MM-DD-YYYY.',
					ageCheck: 'You must be at least 18 years of age.'
				}
			},
			
			submitHandler: function(form) {
				//showThanks();
				
				var email =$("#email").val();
				var fname =$("#fname").val();
				var lname =$("#lname").val();
				var sessid =$("#sessid").val();
				var ip =$("#ip").val();
				var dob =$("#dob").val();
				var phone =$("#phone").val();
				//var optin_news = $('#optin_news').prop('checked') ? 'y' : '';
				var dobFormatted = Date.parse(dob).toString("yyyy-MM-dd");
				var data = 'fname=' + fname + '&lname=' + lname + '&email=' + email + '&phone=' +  phone + '&dob=' + dobFormatted  + '&sessid=' + sessid + '&bonus=' + bns + '&ip=' + ip ;
				
				console.log(data);
				
				$.ajax({
					url: "process-entry.php",	
					type: "GET",
					data: data,
					error: showError,
					dataType: "json",					
					cache: false,			
					success: function (output) {
						if(typeof output.message != "undefined" && String(output.message).match(/email/)){
							alert('Sorry, our records indicate you have already entered once today. Please try again tomorrow.');
							ga('send', 'event', 'Entry', 'Mult_fail');
						}
						else{	//success
							$(':input',form)
								.not(':button, :submit, :reset, :hidden')
								.val('');
							$('#rules').prop('checked', false);
							ga('send', 'event', 'Entry', 'Success');
							//$('#optin_news').prop('checked', false);	
							if(bns){
								$("#bnscpy").html(".  You scored an additional entry today by<br>voting for your Stanley Cup hopeful below.");
							}else{
								entryrow=output.entryid;
							}
							showThanks();
						}
						
						
					}           
				});
				/**/
			}
		});	
	}
	
	return {
		init: initialize
	};
	
})(jQuery);

jQuery(document).ready(nhl.bright.init);