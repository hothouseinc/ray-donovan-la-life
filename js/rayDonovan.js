var errorMsgs = [];
var rooturl = 'http://raydonovanlalife.com';
var bitly = 'http://raydonovanlalife.com';

$( function() {
	$("#enterNow").on('click', function() {
		$("#name, #phone, #email").val("");
		$("#errorMessage").hide();
		$("#entryForm .input span").css('visibility', 'visible');
		showModal( "#entryForm" );


	});

	$("#entryForm_enterNow").on( 'click', function() {
		//hideModal( "#entryForm" );
		ga('send', 'event', 'EntryAttempt', 'RayLASweeps');
		if ( validateForm( "#entry" ) ) 
			processEntry();
	});

	$(".closeBtn").on('click', function() {
		hideModal();
	});

	$( "input" ).on('focus', function() {
		$(this).removeClass('lighter');
		$(this).addClass('darker');
	});

	$("#getMyBonusEntry").on('click', function() {
		ga('send', 'event', 'BonusEntry', 'RayLASweeps');
		updateBonus();

		$("#preClick").hide();
		$("#postClick").show();

		window.open( 'http://www.showtimeanytime.com/', 'Showtime Anytime', null);
	});

	$("#upgradeNow").on('click', function() {
		ga('send', 'event', 'UpgradeNow', 'RayLASweeps');
	});

	$("#watchNow").on('click', function() {
		ga('send', 'event', 'WatchNow', 'RayLASweeps');
	});

	$("#twitIcon").on('click', function() {
		setTweetsw( 'pre' );
		ga('send', 'event', 'TwitterPageShare', 'RayLASweeps');
	});

	$("#ty_Twitter").on('click', function() {
		setTweetsw( 'post' );
		ga('send', 'event', 'TwitterSweepsShare', 'RayLASweeps');
	});

	$("#fbIcon").on('click', function() {
		postToWall( 'pre' );
		ga('send', 'event', 'FBPageShare', 'RayLASweeps');
	});

	$("#ty_Facebook").on('click', function() {
		postToWall( 'post' );
		ga('send', 'event', 'FBSweepsShare', 'RayLASweeps');
	});

	$("#pinterestIcon").on('click', function() {
		pinThis();
	});

	startImageCycle();

});

function validateForm() {
	$("label.input").removeClass("err");
	$("label.input input").removeClass("err");
	errorMsgs = [];

	var valid = true;
	var email =$("#email").val();
	var name =$("#name").val();
	var phone =$("#phone").val();

	if ( email === "" ) {
		$("#email").closest('label.input input').addClass("err");
		addToError( "enter a valid e-mail address" );
		valid = false;
	}
	else if ( !isEmail( email ) ) {
		$("#email").closest('label.input input').addClass("err");
		addToError( "enter a valid e-mail address" );
		valid = false;
	}
	if ( name === "" ) {
		$("#name").closest('label.input input').addClass("err");
		valid = false;
		addToError( "enter a valid first and last name" );
	}
	if ( phone === "" ) {
		$("#phone").closest('label.input input').addClass("err");
		addToError( "enter a valid phone number" );
		valid = false;
	}
	else if ( !isPhone(phone) ) {
		$("#phone").closest('label.input input').addClass("err");
		addToError( "enter a valid phone number" );
		valid = false;
	}

	var agree = $("#agree").prop("checked");
	if ( !agree ) {
		$("#agree").addClass('err');
		$("label[for='agree']").addClass('err');
		addToError( "check that you agree with the official rules." );
		valid = false;
	}
	else {
		$("#agree").removeClass('err');
		$("label[for='agree']").removeClass('err');
	}

	if ( !valid ) {
		$("#errorMessage").html( getErrors() );
		$("#errorMessage").show();
	}
	else {
		$("#errorMessage").hide();
	}
	return valid;
}

function addToError( msg ) {
	errorMsgs.push( msg );
}

function getErrors() {
	var errorMsg = 'Please ';

	if ( errorMsgs.length === 1 ) {
		return errorMsg + " " + errorMsgs[0];
	}
	else {
		for( var i = 0; i < errorMsgs.length; i++ ) {
			if ( errorMsgs.length - 1 === i ) 
				errorMsg += " and " + errorMsgs[i];
			else
				errorMsg += " " + errorMsgs[i] + ", "
		}

		return errorMsg;
	}
}

function isEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function isPhone( phone ) {
	var regex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
	return regex.test( phone );
}

function showModal( divToShow ) {
	$("#modal").fadeIn( function() {
		$( divToShow ).fadeIn();
	} );
}

function hideModal( divToHide ) {
	if ( divToHide === null || divToHide === undefined ) {
		$(".overlay").fadeOut();
		$("#modal").fadeOut();
	}

	$( divToHide ).fadeOut( function() {
		$("#modal").fadeOut();
	} );
}

function processEntry() {

	var agree = $("#agree").prop("checked");
	
	if ( agree ) {
		var email =$("#email").val();
		var name =$("#name").val();
		var phone =$("#phone").val();

		var sessid =$("#sessid").val();
		var ip =$("#ip").val();

		var data = 'name=' + name + '&email=' + email + '&phone=' +  phone + '&sessid=' + sessid  + '&ip=' + ip ;
		//console.log( data );

		$.ajax({
			url: "process-entry.php",	
			type: "GET",
			data: data,
			error: showError,
			dataType: "json",					
			cache: false,			
			success: function (output) {
				if(typeof output.message != "undefined" && String(output.message).match(/email/)){
					alert('Sorry, our records indicate you have already entered once today. Please try again tomorrow.');
				}
				else{	//success
					if ( output !== "No Data" && output.message === "success" ) {
						showModal( "#thankYou" );
						ga('send', 'event', 'EntrySuccess', 'RayLASweeps');
					}
				}
			}           
		});
	}
	else {
		console.log("Not selected");
		//Error message
	}
}

function updateBonus() {
	var agree = $("#agree").prop("checked");
	
	if ( agree ) {
		var email =$("#email").val();
		var name =$("#name").val();
		var phone =$("#phone").val();
		var sessid =$("#sessid").val();
		var ip =$("#ip").val();
		var data = 'name=' + name + '&email=' + email + '&phone=' +  phone + '&sessid=' + sessid  + '&ip=' + ip ;

		$.ajax({
			url: "process-bonus.php",	
			type: "GET",
			data: data,
			error: showError,
			dataType: "json",					
			cache: false,			
			success: function (output) {
				if(typeof output.message != "undefined" && String(output.message).match(/email/)){
					alert('Sorry, our records indicate you have already entered once today. Please try again tomorrow.');
				}
				else{	//success
					if ( output !== "No Data" && output.message === "success" )
						//showModal( "#thankYou" );
						console.log("Change text, bonus applied");
				}
			}           
		});
	}
	else {
		console.log("Not selected");
		//Error message
	}
}

function startImageCycle() {
	var itemList = $(".prizeDesc ul li");
	var count = 0;

	var options = {
		after : function() {
			$( itemList ).css( 'font-weight', 'normal' );
			$( itemList[count] ).css( 'font-weight', 'bold' );
			count++;

			if ( count > itemList.length-1)
				count = 0;
		}
	}
	$("#picRotate").cycle( options );
}

function showError(jqXHR,textStatus,errorThrown){
	console.log("Error: " + jqXHR + "\n " + errorThrown);
	console.log("--------");
	console.log(jqXHR.responseText);
}

function setTweetsw( which ) {    
    twUrl = 'https://twitter.com/intent/tweet?text=';
    if ( which === 'pre')
    	msg = 'Visit RayDonovanLALife.com to enter the #FiOSLALifeSweeps for a chance to win a VIP trip for 2 to L.A. from #SHOWTIME & #VerizonFIOS.';
    else
    	msg = 'I entered the #FiOSLALifeSweeps for a chance to win a VIP trip to L.A. from #SHOWTIME & #VerizonFiOS! Enter now at RayDonovanLALife.com.';

    msg = encodeURIComponent( msg );
    //msg = msg.split(' ').join('%20');
    var  newwindow = window.open(twUrl + msg, "_blank");
}

function postToWall( which ) {
    
	var message = '';
	var picUrl = '';
	var headline = '';

    if ( which === 'pre' ) {
    	picUrl = rooturl+'/images/fbIcon_a.jpg';
    	message = "Visit www.RayDonovanLALife.com to enter for your chance to win weekly prizing plus a VIP trip for 2 to L.A. that will let you stay, shop & dine like Hollywood's elite.";
    	headline = "Live like Ray Donovan - without the drama.";
    }
    else {
    	picUrl = rooturl+'/images/fbIcon_b.jpg';
    	message = "I just entered the #FiOSLALifeSweeps for a chance to win a VIP trip for 2 to L.A. from Verizon FiOS & SHOWTIME&reg;! Enter now at RayDonovanLALife.com and you could live, dine and shop like Hollywood's elite." + bitly;
    	headline = "Ray Donovan L.A. Life Sweepstakes";
    }

    FB.ui({
        'method': 'feed',
        'link': bitly,
        'picture': picUrl,
        'name': headline,
        'description': message
    }),
    function(response) {
    	console.log(response);
    };
   
    return false;
}

function pinThis() {
	ga('send', 'event', 'Pinterest', 'RayLASweeps');
	var desc = encodeURIComponent( "You could win a VIP trip for two to L.A. Enter at www.RayDonovanLALife.com for a chance to dine at Chateau Marmont, shop on Rodeo Drive and more from Showtime&reg; & Verizon FiOS. #FiOSLALifeSweeps" );
	var url = encodeURIComponent( "http://www.raydonovanlalife.com" );
	var img = encodeURIComponent( "http://www.raydonovanlalife.com/images/pinterest_a.jpg ");

	window.open( "http://www.pinterest.com/pin/create/button/?url=" + url + "&media=" + img + "&description=" + desc );
}
